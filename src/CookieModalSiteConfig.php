<?php

namespace Hestec\CookieModal;

use SilverStripe\ORM\DataExtension;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\NumericField;
use SilverStripe\Forms\TreeDropdownField;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\TextField;

class CookieModalSiteConfig extends DataExtension {

    private static $db = array(
        'CmEnable' => 'Boolean',
        'CmExpiration' => 'Int',
        'CmText1' => 'Varchar(100)',
        'CmText2' => 'Varchar(100)',
        'CmStatementText' => 'Varchar(50)',
        'CmAcceptText' => 'Varchar(50)'
    );

    private static $has_one = array(
        'CmStatement' => SiteTree::class
    );

    public function updateCMSFields(FieldList $fields)
    {

        $EnableField = CheckboxField::create('CmEnable', _t("CookieModalSiteConfig.ENABLE", "Enable CookieModal"));
        $CmExpirationField = NumericField::create('CmExpiration', _t("CookieModalSiteConfig.EXPIRATION", "Expiration (days)"));
        $CmExpirationField->setDescription(_t("CookieModalSiteConfig.REMEMBER_DESCRIPTION", "(Remember choice for X days. Default 30 days, if you leave it empty or set 0, it will be 30 days.)"));
        $CmStatementField = TreeDropdownField::create('CmStatementID', _t("CookieBarSiteConfig.COOKIE_STATEMENTPAGE", "Cookie statement page"), SiteTree::class);

        $CmText1Field = TextField::create('CmText1', "Text1");
        $CmText2Field = TextField::create('CmText2', "Text2");
        $CmStatementTextField = TextField::create('CmStatementText', "Statement link text");
        $CmAcceptTextField = TextField::create('CmAcceptText', "Accept link text");

        $fields->addFieldsToTab("Root."._t("CookieModalSiteConfig.COOKIEMODAL", "CookieModal"), array(
            $EnableField,
            $CmExpirationField,
            $CmStatementField,
            $CmText1Field,
            $CmStatementTextField,
            $CmText2Field,
            $CmAcceptTextField
        ));


    }

}