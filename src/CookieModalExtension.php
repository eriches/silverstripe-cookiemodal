<?php

namespace Hestec\CookieModal;

use SilverStripe\Core\Extension;
use SilverStripe\View\Requirements;
use SilverStripe\SiteConfig\SiteConfig;
use SilverStripe\Control\Cookie;

class CookieModalExtension extends Extension
{

    public function onAfterInit(){

        // if CookieModal is enabled and not accepted yet in the CMS site settings, add the js and css
        if ($this->CookieModal() && Cookie::get('CookiesAccepted') != 1) {

            Requirements::customScript($this->CookieModal());

        }

    }

    public function CookieModal(){

        $siteConfig = SiteConfig::current_site_config();
        if ($siteConfig->CmEnable) {

            $expiration = 30;
            if ($siteConfig->CcExpiration <> $expiration && $siteConfig->CcExpiration > 0) {
                $expiration = $siteConfig->CcExpiration;
            }
            $statement = 'none';
            if ($siteConfig->CmStatementID != 0) {
                $statement = $siteConfig->CmStatement()->AbsoluteLink();
            }
            $text1 = 'If you use our site, you will receive';
            if (strlen($siteConfig->CmText1) > 3) {
                $text1 = $siteConfig->CmText1;
            }
            $text2 = 'from us.';
            if (strlen($siteConfig->CmText2) > 3) {
                $text2 = $siteConfig->CmText2;
            }
            $statementtext = 'cookies';
            if (strlen($siteConfig->CmStatementText) > 3) {
                $statementtext = $siteConfig->CmStatementText;
            }
            $accepttext = 'That is fine by me';
            if (strlen($siteConfig->CmAcceptText) > 3) {
                $accepttext = $siteConfig->CmAcceptText;
            }

            return "CookieModal({expiration: $expiration, statement: '$statement', text1: '$text1', text2: '$text2', statementtext: '$statementtext', accepttext: '$accepttext'});";

        }

        return false;

    }

}
