# SilverStripe Cookie Modal #

GDPR EU cookie notice for SilverStripe.

### Requirements ###

SilverStripe 4

### Version ###

Using Semantic Versioning.

### Installation ###

Install via Composer:

composer require "hestec/silverstripe-cookiemodal": "1.*"

### Configuration ###

Add js.cookie.js and CookieModal.js to your main.js project js file under the plugins section.
Add the css to base.scss.
In ss-boilerplate is this already done.

### Usage ###

In your CMS Settings you will find a tab CookieModal where you can enable/disable the cookie notice bar and do some other settings like the link to you privacy policy page.

### Issues ###

No known issues.

### Todo ###
