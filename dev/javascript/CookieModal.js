function CookieModal(config) {

    var expiration = config.expiration;
    var text1 = config.text1;
    var text2 = config.text2;
    var statementtext = config.statementtext;
    var accepttext = config.accepttext;
    var statement = config.statement;
    if (config.statement == 'none'){
        var cookiespage = 'cookies';
    }else{
        var cookiespage = '<a href="' + statement +'">' + statementtext + '</a>';
    };

    $('body').append('<div id="cookie-consent"><p id="cookie-accept" class="text-right">' +  accepttext + '<span class="icon fas fa-times ml-2"></span></p><p class="text-center">' + text1 + ' ' + cookiespage + ' ' + text2 + '</p></div>');

    $('#cookie-accept').on('click', function() {
        Cookies.set('CookiesAccepted', 1, { expires: expiration })
        $('#cookie-consent').hide();
    });

}